from selenium import webdriver
from datetime import datetime


if __name__ == "__main__":

    start_page = 'https://www.quegroup.org'
    cutoff = datetime(year=2017, month=6, day=5)
    # login_url = "https://www.quegroup.org/Sys/Login"

    browser = webdriver.Chrome()
    browser.get('https://www.quegroup.org/forum')

    # log in
    que_username = ''  # put in your quegroup username here
    que_password = ''  # put in your quegroup password here
    browser.find_element_by_id('ctl00_ContentArea_loginViewControl_loginControl_userName').send_keys(que_username)
    browser.find_element_by_id('ctl00_ContentArea_loginViewControl_loginControl_Password').send_keys(que_password)
    browser.find_element_by_id('ctl00_ContentArea_loginViewControl_loginControl_Login').click()

    # get all forum links

    forums = browser.find_elements_by_css_selector('a.forumTitle')

    forums_list = []

    for forum in forums:
        href = forum.get_attribute('href')
        desc = forum.text
        forums_list.append([href, desc])

    print(forums_list)

    posts = []
    for forum in forums_list:
        print('Going to forum {0}...'.format(forum[1]))
        page_num = 1

        loop_break = False
        while not loop_break:
            try:
                browser.get('{0}?tpg={1}'.format(forum[0], page_num))
            except:
                break

            topics = browser.find_elements_by_css_selector('tr.topicListRow')

            len_topics = len(topics)

            for topic in topics:
                # print(topic.text)
                try:
                    print('date: {0}'.format(topic.find_element_by_class_name('lastReplyDate').get_attribute('text')))
                    post_date = datetime.strptime(topic.find_element_by_class_name('lastReplyDate').get_attribute('text'), '%d %b %Y %I:%M %p')
                except Exception as e:
                    print('post_date error, type {0}, exception: {1}'.format(type(e), e))
                if post_date > cutoff:
                    href = topic.find_element_by_class_name('topicTitle').get_attribute('href')
                    posts.append(href)
                else:
                    loop_break = True
                    break

            if len_topics < 10:
                loop_break = True

            page_num += 1


    print('Number of posts to parse: {0}'.format(len(posts)))

    user_count_dict = {}

    for post in posts:
        page_num = 1
        print('Getting post: {0}, page {1}'.format(post, page_num))

        loop_break = False

        while not loop_break:
            try:
                browser.get('{0}?mlpg={1}'.format(post, page_num))
            except:
                break

            messages = browser.find_elements_by_class_name('boxesListItem')
            len_messages = len(messages)

            for message in messages:
                date_posted = datetime.strptime(message.find_element_by_class_name('messageCreatedDate').text,
                                  '%d %b %Y %I:%M %p')
                if date_posted > cutoff:
                    poster = message.find_element_by_class_name('forumMessageTable').find_element_by_class_name('inner').text
                    print('Post: {0}, poster: {1}, date: {2}'.format(post, poster, date_posted))
                    if poster in user_count_dict:
                        user_count_dict[poster] += 1
                    else:
                        user_count_dict[poster] = 1

            page_num += 1

            if len_messages < 10:
                loop_break = True
            else:
                pages = browser.find_element_by_class_name('forumPagerContainer').find_elements_by_class_name('near')
                if pages:
                    loop_break = True
                    for page in pages:
                        href = page.get_attribute('href')
                        if href:
                            if href.endswith('?mlpg={0}'.format(page_num)):
                                loop_break = False
                else:
                    loop_break = True

    for key, value in user_count_dict.items():
        print('User: {0}, Messages: {1}'.format(key, value))
